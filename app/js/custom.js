$(document).ready(function(){
  $('.slide-partner').slick({
   		dots: true,
		infinite: true,
		autoplay:true,
		speed: 300,
		slidesToShow: 6,
		slidesToScroll: 1,
		responsive: [
		    {
		      	breakpoint: 1024,
		      	settings: {
			        slidesToShow: 4,
			        infinite: true,
			        dots: true
		      	}
		    },
		    {
			    breakpoint: 600,
			    settings: {
			        slidesToShow: 2,
			    }
		    },
		    {
		      	breakpoint: 480,
		      	settings: {
			        slidesToShow: 1
		      	}
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
  	});
  	$( window ).scroll(function() {
  		var scroll = $(window).scrollTop();

	    if (scroll >= 100) {
	    	$( "#header" ).addClass('header-fix');
	    } else {
	    	$( "#header" ).removeClass('header-fix');
	    }
	  
	});

  	$(document).on('click', 'a', function(event){
	    event.preventDefault();

	    $('html, body').animate({
	        scrollTop: ($( $.attr(this, 'href') ).offset().top - 65)
	    }, 500);
	});

	// menu
	$('.navbar-main-left > li').click(function(){
		$('.navbar-main-left > li').removeClass('active');
		$(this).addClass('active');
	});
});
